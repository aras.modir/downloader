package com.aras.modir.downloader;

public class DownloadModelPOJO {
    int percent;

    public DownloadModelPOJO(int percent) {
        this.percent = percent;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }
}
