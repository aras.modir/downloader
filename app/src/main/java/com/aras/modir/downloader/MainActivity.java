package com.aras.modir.downloader;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MainActivity extends AppCompatActivity {
    EditText url;
    ProgressBar progressBar;
    TextView progressText;

    // this is new comments

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        url = findViewById(R.id.url);
        progressBar = findViewById(R.id.progrss);
        progressText = findViewById(R.id.progress_text);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String urlValue = url.getText().toString();

                Intent intent = new Intent(MainActivity.this, DownloaderService.class);
                intent.putExtra("url", urlValue);
                startService(intent);
            }
        });

    }

    @Subscribe
    public void onDownloading(DownloadModelPOJO modelPOJO) {
        progressBar.setProgress(modelPOJO.getPercent());
        progressText.setText(modelPOJO.getPercent() + "%");
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(MainActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(MainActivity.this);
    }
}
