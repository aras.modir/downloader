package com.aras.modir.downloader;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import cz.msebera.android.httpclient.Header;

public class DownloaderService extends Service {
    String TAG = "downloader_";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: ");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String url = intent.getStringExtra("url");
        Toast.makeText(this, url, Toast.LENGTH_SHORT).show();
        download(url);
        Log.d(TAG, "onStartCommand:");
        return super.onStartCommand(intent, flags, startId);
    }

    void download(String url) {
        AsyncHttpClient client = new AsyncHttpClient();

        final DownloadModelPOJO modelPOJO = new DownloadModelPOJO(0);

        client.get(url, new FileAsyncHttpResponseHandler(this) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Toast.makeText(DownloaderService.this, throwable.toString(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onFailure: " + throwable);
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
                int percentage = (int) (bytesWritten * 100.0 / totalSize + 0.5);

                modelPOJO.setPercent(percentage);
                EventBus.getDefault().post(modelPOJO);

                Log.d(TAG, "onProgress: " + percentage);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                Log.d(TAG, "onProgress: "
                + file.getAbsolutePath() + " " + file.length());
            }
        });
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
